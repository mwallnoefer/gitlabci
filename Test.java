public class Test
{
	public static void main(String[] args) {
		// Works when parameter "ok" is passed, otherwise yields an exit code <> 0
		if (args.length != 1 || !args[0].equals("ok")) {
			System.err.println("Error");
			System.exit(1); // Exit code 1
		}
		System.out.println("Ok");
	}
}
